﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DziekanatMVC.Startup))]
namespace DziekanatMVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
