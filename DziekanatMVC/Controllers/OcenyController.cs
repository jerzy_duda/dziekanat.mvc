﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DziekanatMVC.Models;

namespace DziekanatMVC.Controllers
{
    public class OcenyController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Oceny
        public ActionResult Index()
        {
            var oceny = db.Oceny.Include(o => o.Przedmioty).Include(o => o.Student);
            return View(oceny.ToList());
        }

        // GET: Oceny/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oceny oceny = db.Oceny.Find(id);
            if (oceny == null)
            {
                return HttpNotFound();
            }
            return View(oceny);
        }

        // GET: Oceny/Create
        public ActionResult Create()
        {
            ViewBag.KodPrzedmiotu = new SelectList(db.Przedmiots, "KodPrzedmiotu", "NazwaPrzedmiotu");
            ViewBag.Indeks = new SelectList(db.Students, "Indeks", "ImięINazwisko");
            return View();
        }

        // POST: Oceny/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Indeks,KodPrzedmiotu,Data,Ocena")] Oceny oceny)
        {
            if (ModelState.IsValid)
            {
                db.Oceny.Add(oceny);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.KodPrzedmiotu = new SelectList(db.Przedmiots, "KodPrzedmiotu", "NazwaPrzedmiotu", oceny.KodPrzedmiotu);
            ViewBag.Indeks = new SelectList(db.Students, "Indeks", "ImięINazwisko", oceny.Indeks);
            return View(oceny);
        }

        // GET: Oceny/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oceny oceny = db.Oceny.Find(id);
            if (oceny == null)
            {
                return HttpNotFound();
            }
            ViewBag.KodPrzedmiotu = new SelectList(db.Przedmiots, "KodPrzedmiotu", "NazwaPrzedmiotu", oceny.KodPrzedmiotu);
            ViewBag.Indeks = new SelectList(db.Students, "Indeks", "ImięINazwisko", oceny.Indeks);
            return View(oceny);
        }

        // POST: Oceny/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Indeks,KodPrzedmiotu,Data,Ocena")] Oceny oceny)
        {
            if (ModelState.IsValid)
            {
                db.Entry(oceny).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.KodPrzedmiotu = new SelectList(db.Przedmiots, "KodPrzedmiotu", "NazwaPrzedmiotu", oceny.KodPrzedmiotu);
            ViewBag.Indeks = new SelectList(db.Students, "Indeks", "ImięINazwisko", oceny.Indeks);
            return View(oceny);
        }

        // GET: Oceny/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Oceny oceny = db.Oceny.Find(id);
            if (oceny == null)
            {
                return HttpNotFound();
            }
            return View(oceny);
        }

        // POST: Oceny/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Oceny oceny = db.Oceny.Find(id);
            db.Oceny.Remove(oceny);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
