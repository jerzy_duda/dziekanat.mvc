﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DziekanatMVC.Models;
using PagedList;

namespace DziekanatMVC.Controllers
{
    public class StudenciController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Studenci
        public ActionResult Index(String sortOrder, string searchString, int? page)
        {
            ViewBag.IndeksSortParm = String.IsNullOrEmpty(sortOrder) ? "indeks_desc" : "";
            ViewBag.NazwiskoSortParm = sortOrder == "nazwisko_asc" ? "nazwisko_desc" : "nazwisko_asc";
            var studenci = from s in db.Students select s;
            switch (sortOrder)
            {
                case "nazwisko_desc": studenci = studenci.OrderByDescending(s => s.Nazwisko); break;
                case "nazwisko_asc": studenci = studenci.OrderBy(s => s.Nazwisko); break;
                case "indeks_desc": studenci = studenci.OrderByDescending(s => s.Indeks); break;
                default: studenci = studenci.OrderBy(s => s.Indeks); break;
            }
            if (!String.IsNullOrEmpty(searchString))
            {
                studenci = studenci.Where(s => s.Nazwisko.Contains(searchString));
            }
            int pageSize = 3;
            int pageNumber = (page ?? 1);
            return View(studenci.ToPagedList(pageNumber, pageSize));
        }

        // GET: Studenci/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = db.Students.Find(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            var oceny = db.Oceny.Where(x => x.Indeks == student.Indeks);
            double suma = 0;
            foreach (var item in oceny)
                suma += item.Ocena;
            ViewBag.Srednia = suma / oceny.Count();
            return View(student);
        }

        // GET: Studenci/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Studenci/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Indeks,Imię,Nazwisko")] Student student)
        {
            if (ModelState.IsValid)
            {
                db.Students.Add(student);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(student);
        }

        // GET: Studenci/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = db.Students.Find(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // POST: Studenci/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Indeks,Imię,Nazwisko")] Student student)
        {
            if (ModelState.IsValid)
            {
                db.Entry(student).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(student);
        }

        // GET: Studenci/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Student student = db.Students.Find(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            return View(student);
        }

        // POST: Studenci/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Student student = db.Students.Find(id);
            db.Students.Remove(student);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
