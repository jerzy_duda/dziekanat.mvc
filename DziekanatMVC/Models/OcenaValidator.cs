﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DziekanatMVC.Models
{
    public class OcenaValidator : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            double ocena = (double)value;
            if ((ocena * 10) % 5 == 0 && ocena >= 2.0 && ocena <= 5.0) //logikę trzeba dokończyć
            {
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult("To nie jest prawidłowa ocena!");
            }
        }
    }
}