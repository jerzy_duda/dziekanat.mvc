﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DziekanatMVC.Models
{
    public class Student
    {
        [Key]
        public int Indeks { get; set; }
        public string Imię { get; set; }
        public string Nazwisko { get; set; }
        public string ImięINazwisko
        {
            get
            {
                return Imię + " " + Nazwisko;
            }
        }
        public virtual ICollection<Oceny> Oceny { get; set; }
    }
}