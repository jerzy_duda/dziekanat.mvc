﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DziekanatMVC.Models
{
    public class Oceny
    {
        public int Id { get; set; }
        public int Indeks { get; set; }
        public int KodPrzedmiotu { get; set; }
        public DateTime Data { get; set; }
        [Required(ErrorMessage ="Wpisz datę")]
        [OcenaValidator]
        public double Ocena { get; set; }

        public virtual Student Student { get; set; }
        public virtual Przedmiot Przedmioty { get; set; }
    }
}