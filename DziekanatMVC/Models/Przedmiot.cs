﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DziekanatMVC.Models
{
    public class Przedmiot
    {
        [Key]
        public int KodPrzedmiotu { set; get; }
        [DisplayName("Przedmiot")]
        [MinLength(5)]
        public string NazwaPrzedmiotu { set; get; }
        public virtual ICollection<Oceny> Oceny { get; set; }
    }
}